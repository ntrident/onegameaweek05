using System.IO;
using TMPro;
using UnityEngine;

public class HighScore : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI textMesh;
    //[SerializeField]
    //private Vector3 punch;
    public int currentHighScore;
    public float WorldTimer;
    private float timer;
    private int score = 1;
    private HighScoreData highScoreData;
    private int loadedHighScore;
    private string filePath;

    private void Awake()
    {
        filePath = Application.persistentDataPath + "/HighScore.json";
    }

    private void Update()
    {
        textMesh.text = currentHighScore.ToString();

        WorldTimer += Time.deltaTime;
        timer += Time.deltaTime;

        if (timer >= 1f)
        {

            AddHighScore(score);


            //Reset the timer to 0.
            timer = 0;
        }
    }

    public void AddHighScore(int score)
    {
        currentHighScore += score;



        //if (DOTween.IsTweening(transform))    
        //{
        //transform.DOComplete();
        //}

        //transform.DOPunchScale(punch, 0.25f);
    }

    public void SaveHighScore()
    {
        if (File.Exists(filePath))
        {
            string loadedHighScoreData = File.ReadAllText(filePath);
            highScoreData = JsonUtility.FromJson<HighScoreData>(loadedHighScoreData);

            loadedHighScore = highScoreData.highScore;

            if (currentHighScore > highScoreData.highScore)
            {
                string filePath = Application.persistentDataPath + "/HighScore.json";

                HighScoreData highScoreData = new HighScoreData();

                highScoreData.highScore = currentHighScore;

                string dataToSave = JsonUtility.ToJson(highScoreData);

                File.WriteAllText(filePath, dataToSave);
            }
        }

        else
        {
            string filePath = Application.persistentDataPath + "/HighScore.json";

            HighScoreData highScoreData = new HighScoreData();

            highScoreData.highScore = currentHighScore;

            string dataToSave = JsonUtility.ToJson(highScoreData);

            File.WriteAllText(filePath, dataToSave);
        }
    }
}