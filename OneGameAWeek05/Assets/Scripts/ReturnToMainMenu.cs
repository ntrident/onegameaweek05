using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace SaveScore
{
    public class ReturnToMainMenu : MonoBehaviour
    {
        /// <summary>
        /// Serialized reference to the HighScore, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private HighScore highScore;

        /// <summary>
        /// Serialized string value, name of the MainMenu Scene, assign this in the Inspector
        /// </summary>
        [SerializeField]
        private string mainMenuSceneName;

        private void Update()
        {
            // We check if the Escape-Key on the keyboard was hit
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //MainMenu();
            }
        }

        public void MainMenu()
        {
            // If we got a new highscore we call the SaveHighScore() method of our highScore
            highScore.SaveHighScore();

            //waitToReturn();

            // After the save operation we load our MainMenu Scene by its name
            SceneManager.LoadScene(mainMenuSceneName);
        }

        //IEnumerator waitToReturn()
        //{
            //yield return new WaitForSeconds(3);
        //}
    }
}